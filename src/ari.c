#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <inttypes.h>
#include "ari.h"

#define TABLES_NUM 1
const int NOT_EOF = -1 * ((EOF == -1) + 1);

void initialize_table (int64_t *table) {
    for (int i = 0; i <= 256; i++)
            table[i] = i+1;
}

void cut_table (int64_t *table) {
    int buf1 = 0, buf2 = 0;
    for (int i = 0; i <= 255; i++) {
        buf2 = table[i+1];
        if ((table[i+1] - table[i]) > 1)
            table[i+1] = table[i] + (table[i+1] - buf1) / 2;

        buf1 = buf2;
    }
}

void modify_table_add (int64_t *table, int64_t max_weight, int c, int64_t aggressiveness) {
    for (int i = c+1; i <= 256; i++)
        table[i] += aggressiveness;

    while (table[256] > max_weight)
        cut_table (table);
}

void write_complete_byte (FILE *f, int *bits) {
    char byte = bits[0];
    //printf (" %d", bits[0]);
    for (int i = 1; i < 8; i++) {
        //printf ("%d", bits[i]);
        byte = byte << 1;
        byte = byte | bits[i];
    }
    //printf ("byte %d%d%d%d%d%d%d%d\n", (byte & 128) > 0, (byte & 64) > 0, (byte & 32) > 0, (byte & 16) > 0, (byte & 8) > 0, (byte & 4) > 0, (byte & 2) > 0, (byte & 1) > 0);
    fputc (byte, f);
}

void write_bits_plus_follow (FILE *f, int bit, int *bits_to_follow, int write_everythig_left) {
    static int previous_bits[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    static int previous_bits_num = 0;

    previous_bits[previous_bits_num] = bit;
    //printf ("%d", bit);
    previous_bits_num++;

    if (previous_bits_num == 8) {
        write_complete_byte (f, previous_bits);
        previous_bits_num = 0;
    }

    for (; *bits_to_follow > 0; *bits_to_follow = *bits_to_follow - 1) {
        previous_bits[previous_bits_num] = (!bit);
        //printf ("%d", !bit);
        previous_bits_num++;
        if (previous_bits_num == 8) {
            write_complete_byte (f, previous_bits);
            previous_bits_num = 0;
        }
    }

    if (write_everythig_left) {
        if (previous_bits_num) {
            for (int i = 8; i > previous_bits_num; i--)
                previous_bits[i-1] = 0;

            write_complete_byte(f, previous_bits);
        }
        //printf ("I wrote %d extra zeros", previous_bits_num);
        return;
    }
}

void compress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    int64_t li = 0, hi = 4294967295, l = li, h = hi; /** hi = 2^32 - 1 */
    int64_t First_qtr = (hi + 1) / 4, Second_qtr = First_qtr * 2, Third_qtr = First_qtr * 3;
    int64_t tables[TABLES_NUM][257]; /** (my eof + 256 symbols) probability gapes = 257 variables per table*/
    int64_t max_weight = (hi + 1) / 4;
    for (int i = 0; i < TABLES_NUM; i++)
        initialize_table(tables[i]);
    int current_table = 0;
    int bits_to_follow = 0;
    int c;
    while ((c = fgetc (ifp)) != EOF) {
        l = li + (((hi - li + 1) * tables[current_table][c]) / tables[current_table][256]);
        h = li + (((hi - li + 1) * tables[current_table][c+1]) / tables[current_table][256]) - 1;
        //printf ("%ld %ld\n", l, h);
        li = l;
        hi = h;

        for (int i = 0; i < TABLES_NUM; i++)
            modify_table_add (tables[i], max_weight, c, 5);

        while (1) {
            if (hi < Second_qtr) {
                write_bits_plus_follow (ofp, 0, &bits_to_follow, 0);
            }
            else if (li >= Second_qtr){
                write_bits_plus_follow (ofp, 1, &bits_to_follow, 0);
                li -= Second_qtr;
                hi -= Second_qtr;
            }
            else if ((li >= First_qtr) && (hi < Third_qtr)){
                bits_to_follow++;
                li -= First_qtr;
                hi -= First_qtr;
            }
            else{
                break;
            }
            li += li;
            hi += hi + 1;
        }
    }


    hi = li + (((hi - li + 1) * tables[current_table][0]) / tables[current_table][256]) - 1;

    while (1) {
        if (hi < Second_qtr) {
            write_bits_plus_follow (ofp, 0, &bits_to_follow, 0);
            li *= 2;
            hi *= 2;
        }
        else if (li >= Second_qtr){
            write_bits_plus_follow (ofp, 1, &bits_to_follow, 0);
            li -= Second_qtr;
            hi -= Second_qtr;
            li *= 2;
            hi *= 2;
        }
        else if ((li >= First_qtr) && (hi < Third_qtr)){
            bits_to_follow++;
            li -= First_qtr;
            hi -= First_qtr;
            li *= 2;
            hi *= 2;
        }
        else{
            break;
        }
    }

    write_bits_plus_follow (ofp, 1, &bits_to_follow, 1);

    fclose(ifp);
    fclose(ofp);
}

int get_symbol (int64_t point, int64_t l, int64_t h, int64_t *table) {
    if ((point >= l) && (point < (l + (h - l + 1) * table[0] / table[256]))){
        //printf ("EOF %ld <= %ld < %ld\n", l, point, (l + (h - l + 1) * table[0] / table[256]));
        return EOF;
    }

    for (int i = 0; i <= 255; i++) {
        if (point < (l + (h - l + 1) * table[i+1] / table[256])) { //if (pointl >= (l + (h - l + 1) * table[i] / table[256]))
            if (point >= (l + (h - l + 1) * table[i] / table[256])) {
                //printf ("YES %ld %ld %ld %ld, %ld %ld %ld\n", pointl, pointh, (l + (h - l + 1) * table[i] / table[256]), (l + (h - l + 1) * table[i+1] / table[256]), table[i], table[i+1], table[256]);
                //printf ("YES %ld <= %ld < %ld          %c (=%d)\n", (l + (h - l + 1) * table[i] / table[256]), point, (l + (h - l + 1) * table[i+1] / table[256]) - 1, i, i);
                return i;
            }
            else
                return NOT_EOF;
        }
    }
    return NOT_EOF;
}

void decompress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    int64_t li = 0, hi = 4294967295/*4611686018427387903*/, l = li, h = hi; /** hi = 2^32 - 1 */
    int64_t point = 0;
    int64_t First_qtr = (hi + 1) / 4, Second_qtr = First_qtr * 2, Third_qtr = First_qtr * 3;
    int64_t tables[TABLES_NUM][257]; /** (my eof + 256 symbols) probability gapes = 257 variables per table*/
    int64_t max_weight = (hi + 1) / 4;
    for (int i = 0; i < TABLES_NUM; i++)
        initialize_table(tables[i]);
    int current_table = 0;
    int bits_to_follow = 0;

    int byte = fgetc (ifp), c;
    for (int i = 0; i < 32; i++) {
        if (byte != EOF) {
            point += point + ((byte >> (7 - i % 8)) & 1);
        }
        else {
            point += point;
        }
        //printf ("i=%d: %d - %ld\n",i, ((byte >> (7 - i % 8)) & 1),point);
        if ((i % 8) == 7)
            byte = fgetc (ifp);
    }

    int last_byte = byte;
    int i = 0;
    while ((last_byte != EOF) || (c != EOF)) {
        c = get_symbol (point, li, hi, tables[current_table]);
        if (c == EOF)
            break;

        l = li + (((hi - li + 1) * tables[current_table][c]) / tables[current_table][256]);
        h = li + (((hi - li + 1) * tables[current_table][c+1]) / tables[current_table][256]) - 1;
        li = l;
        hi = h;
        for (int i = 0; i < TABLES_NUM; i++)
            modify_table_add (tables[i], max_weight, c, 5);

        while (1) {
            if (hi < Second_qtr) {
                ;
            }
            else if (li >= Second_qtr) {
                li -= Second_qtr;
                hi -= Second_qtr;
                point -= Second_qtr;
            }
            else if ((li >= First_qtr) && (hi < Third_qtr)) {
                li -= First_qtr;
                hi -= First_qtr;
                point -= First_qtr;
            }
            else {
                break;
            }
            li += li;
            hi += hi + 1;
            point += point + ((byte >> (7 - i)) & 1);
            //printf ("bit %d (i=%d), point %ld\n", ((byte >> (7 - i)) & 1), i+1, point);
            i++;
            if ((i % 8) == 0) {
                byte = fgetc(ifp);
                if (byte == EOF) {
                    last_byte = byte;
                    byte = 0;
                }
                i = 0;
                //printf("reading byte %d\n", byte);
            }
        }

        fputc (c, ofp);
    }

    fclose(ifp);
    fclose(ofp);
}
