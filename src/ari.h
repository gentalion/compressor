#pragma once

void initialize_table (int64_t *table);
void cut_table (int64_t *table);
void modify_table_add (int64_t *table, int64_t max_weight, int c, int64_t agressiveness);
void write_complete_byte (FILE *f, int *bits);
void write_bits_plus_follow (FILE *f, int bit, int *bits_to_follow, int write_everythig_left);
void compress_ari(char *ifile, char *ofile);
int get_symbol (int64_t point, int64_t l, int64_t h, int64_t *table);
void decompress_ari(char *ifile, char *ofile);
