#pragma once

struct branch {
    int64_t weight;
    int64_t total_weight;
    struct branch **table;
};
typedef struct branch* tree;

tree initialize_tree ();
tree initialize_leaf (int64_t n);
void update_tree (tree *t, int depth, int *key, int aggressiveness);
void update_tree_fixed (tree *t, int depth, int *key, int aggressiveness);
int64_t get_border (tree t, int depth, int *key, int key_last);
int64_t get_border_fixed (tree t, int depth, int *key, int key_last);
int64_t get_total_weight (tree t, int depth, int *key);
int64_t get_total_weight_fixed (tree t, int depth, int *key);
void initialize_table_ppm (int64_t *table);
void cut_table_ppm (int64_t *table);
void modify_table_add_ppm (int64_t *table, int64_t max_weight, int c, int64_t agressiveness);
void write_complete_byte_ppm (FILE *f, int *bits);
void write_bits_plus_follow_ppm (FILE *f, int bit, int *bits_to_follow, int write_everythig_left);
void compress_ppm(char *ifile, char *ofile);
int get_symbol_ari (int64_t point, int64_t l, int64_t h, int64_t *table);
int get_symbol_ppm (int64_t point, int64_t l, int64_t h, tree t, int *key);
void decompress_ppm(char *ifile, char *ofile);
