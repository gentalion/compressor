#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "ppm.h"

#define DEPTH 4

const int NOT_EOF_PPM = -1 * ((EOF == -1) + 1);

tree initialize_tree () {
    tree a = (tree) malloc (sizeof(struct branch));
    a->weight = 1;
    a->total_weight = 257;
    a->table = (tree*) calloc (257, sizeof(tree));
}

tree initialize_leaf (int64_t n) {
    tree a = (tree) malloc (sizeof(struct branch));
    a->weight = n;
    a->total_weight = 0;
    a->table = NULL;
}

void update_tree (tree *t, int depth, int *key, int aggressiveness) {
    tree cur = *t;

    for (int i = 0; i < depth - 1; i++) {
        cur->total_weight = cur->total_weight + aggressiveness;
        if (cur->table[key[i]+1] == NULL) {
            cur->table[key[i]+1] = initialize_tree ();
            cur = cur->table[key[i]+1];
        }
        else {
            cur = cur->table[key[i]+1];
        }
    }

    cur->total_weight = cur->total_weight + aggressiveness;
    if (cur->table[key[depth - 1]+1] == NULL) {
        cur->table[key[depth - 1]+1] = initialize_leaf (aggressiveness);
    }
    else {
        cur = cur->table[key[depth - 1]+1];
        cur->weight = cur->weight + aggressiveness;
    }
}

void update_tree_fixed (tree *t, int depth, int *key, int aggressiveness) {
    for (int i = depth; i > 0; i--) {
        update_tree(t, i, key, aggressiveness);
    }
}

int64_t get_border (tree t, int depth, int *key, int key_last) {
    tree cur = t;

    for (int i = 0; i < depth - 1; i++) {
        if (cur->table[key[i]+1] == NULL) {
            return key_last + 1;
        }
        else {
            cur = cur->table[key[i]+1];
        }
    }

    int sum = 0;
    for (int i = 0; i <= key_last; i++) {
        if (cur->table[i] == NULL) {
            sum += 1;
        }
        else {
            sum += cur->table[i]->weight;
        }
    }

    return sum;
}

int64_t get_border_fixed (tree t, int depth, int *key, int key_last) {
    int64_t sum = 0;
    for (int i = depth; i > 0; i--) {
        sum += get_border (t, i, key, key_last);
    }

    return sum;
}

int64_t get_total_weight (tree t, int depth, int *key) {
    tree cur = t;

    for (int i = 0; i < depth - 1; i++) {
        if (cur->table[key[i]+1] == NULL) {
            return 257;
        }
        else {
            cur = cur->table[key[i]+1];
        }
    }

    return cur->total_weight;
}

int64_t get_total_weight_fixed (tree t, int depth, int *key) {
    int64_t sum = 0;
    for (int i = depth; i > 0; i--) {
        sum += get_total_weight (t, i, key);
    }

    return sum;
}

void initialize_table_ppm (int64_t *table) {
    for (int i = 0; i <= 256; i++)
            table[i] = i+1;
}

void cut_table_ppm (int64_t *table) {
    int buf1 = 0, buf2 = 0;
    for (int i = 0; i <= 255; i++) {
        buf2 = table[i+1];
        if ((table[i+1] - table[i]) > 1)
            table[i+1] = table[i] + (table[i+1] - buf1) / 2;

        buf1 = buf2;
    }
}

void modify_table_add_ppm (int64_t *table, int64_t max_weight, int c, int64_t aggressiveness) {
    for (int i = c+1; i <= 256; i++)
        table[i] += aggressiveness;

    while (table[256] > max_weight)
        cut_table_ppm (table);
}

void write_complete_byte_ppm (FILE *f, int *bits) {
    char byte = bits[0];
    //printf (" %d", bits[0]);
    for (int i = 1; i < 8; i++) {
        //printf ("%d", bits[i]);
        byte = byte << 1;
        byte = byte | bits[i];
    }
    //printf ("byte %d%d%d%d%d%d%d%d\n", (byte & 128) > 0, (byte & 64) > 0, (byte & 32) > 0, (byte & 16) > 0, (byte & 8) > 0, (byte & 4) > 0, (byte & 2) > 0, (byte & 1) > 0);
    fputc (byte, f);
}

void write_bits_plus_follow_ppm (FILE *f, int bit, int *bits_to_follow, int write_everythig_left) {
    static int previous_bits[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    static int previous_bits_num = 0;

    previous_bits[previous_bits_num] = bit;
    //printf ("%d", bit);
    previous_bits_num++;

    if (previous_bits_num == 8) {
        write_complete_byte_ppm (f, previous_bits);
        previous_bits_num = 0;
    }

    for (; *bits_to_follow > 0; *bits_to_follow = *bits_to_follow - 1) {
        previous_bits[previous_bits_num] = (!bit);
        //printf ("%d", !bit);
        previous_bits_num++;
        if (previous_bits_num == 8) {
            write_complete_byte_ppm (f, previous_bits);
            previous_bits_num = 0;
        }
    }

    if (write_everythig_left) {
        if (previous_bits_num) {
            for (int i = 8; i > previous_bits_num; i--)
                previous_bits[i-1] = 0;

            write_complete_byte_ppm (f, previous_bits);
        }
        //printf ("I wrote %d extra zeros", previous_bits_num);
        return;
    }
}

void compress_ppm(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    int64_t li = 0, hi = 4294967295, l = li, h = hi; /** hi = 2^32 - 1 */
    int64_t First_qtr = (hi + 1) / 4, Second_qtr = First_qtr * 2, Third_qtr = First_qtr * 3;
    int64_t table[257]; /** (my eof + 256 symbols) probability gapes = 257 variables per table*/
    int64_t max_weight = (hi + 1) / 4;
    initialize_table_ppm (table);
    tree model = initialize_tree ();
    int bits_to_follow = 0;

    int c;
    int64_t c_num = 0;
    int c_buf[4] = {-1, -1, -1, -1};

    while ((c = fgetc (ifp)) != EOF) {
        if (c_num < DEPTH - 1) {
            l = li + (((hi - li + 1) * table[c]) / table[256]);
            h = li + (((hi - li + 1) * table[c+1]) / table[256]) - 1;
            //printf ("%ld %ld\n", l, h);
            li = l;
            hi = h;
            c_buf[c_num] = c;
            c_num++;
        }
        else {
            c_buf[3] = c;
            l = li + (((hi - li + 1) * get_border (model, DEPTH, c_buf, c)) / get_total_weight (model, DEPTH, c_buf));
            h = li + (((hi - li + 1) * get_border (model, DEPTH, c_buf, c+1)) / get_total_weight (model, DEPTH, c_buf)) - 1;
            //printf ("%ld %ld       %ld %ld %ld\n", l, h, get_border (model, c_buf, c), get_border (model, c_buf, c+1), get_total_weight (model, c_buf));
            li = l;
            hi = h;

            update_tree (&model, DEPTH, c_buf, 5);

            for (int i = 0; i < DEPTH - 1; i++)
                c_buf[i] = c_buf[i+1];
        }

        while (1) {
            if (hi < Second_qtr) {
                write_bits_plus_follow_ppm (ofp, 0, &bits_to_follow, 0);
            }
            else if (li >= Second_qtr){
                write_bits_plus_follow_ppm (ofp, 1, &bits_to_follow, 0);
                li -= Second_qtr;
                hi -= Second_qtr;
            }
            else if ((li >= First_qtr) && (hi < Third_qtr)){
                bits_to_follow++;
                li -= First_qtr;
                hi -= First_qtr;
            }
            else{
                break;
            }
            li += li;
            hi += hi + 1;
        }
    }

    if (c_num < DEPTH - 1) {
        hi = li + (((hi - li + 1) * table[0]) / table[256]) - 1;
    }
    else {
        hi = li + (((hi - li + 1) * get_border (model, DEPTH, c_buf, 0)) / get_total_weight (model, DEPTH, c_buf)) - 1;
    }

    while (1) {
        if (hi < Second_qtr) {
            write_bits_plus_follow_ppm (ofp, 0, &bits_to_follow, 0);
            li *= 2;
            hi *= 2;
        }
        else if (li >= Second_qtr){
            write_bits_plus_follow_ppm (ofp, 1, &bits_to_follow, 0);
            li -= Second_qtr;
            hi -= Second_qtr;
            li *= 2;
            hi *= 2;
        }
        else if ((li >= First_qtr) && (hi < Third_qtr)){
            bits_to_follow++;
            li -= First_qtr;
            hi -= First_qtr;
            li *= 2;
            hi *= 2;
        }
        else{
            break;
        }
    }

    write_bits_plus_follow_ppm (ofp, 1, &bits_to_follow, 1);

    fclose(ifp);
    fclose(ofp);
}

int get_symbol_ari (int64_t point, int64_t l, int64_t h, int64_t *table) {
    if ((point >= l) && (point < (l + (h - l + 1) * table[0] / table[256]))){
        //printf ("EOF %ld <= %ld < %ld\n", l, point, (l + (h - l + 1) * table[0] / table[256]));
        return EOF;
    }

    for (int i = 0; i <= 255; i++) {
        if (point < (l + (h - l + 1) * table[i+1] / table[256])) { //if (pointl >= (l + (h - l + 1) * table[i] / table[256]))
            if (point >= (l + (h - l + 1) * table[i] / table[256])) {
                //printf ("YES %ld %ld %ld %ld, %ld %ld %ld\n", pointl, pointh, (l + (h - l + 1) * table[i] / table[256]), (l + (h - l + 1) * table[i+1] / table[256]), table[i], table[i+1], table[256]);
                //printf ("YES %ld <= %ld < %ld          %c (=%d)\n", (l + (h - l + 1) * table[i] / table[256]), point, (l + (h - l + 1) * table[i+1] / table[256]) - 1, i, i);
                return i;
            }
            else
                return NOT_EOF_PPM;
        }
    }
    return NOT_EOF_PPM;
}

int get_symbol_ppm (int64_t point, int64_t l, int64_t h, tree t, int *key) {
    if ((point >= l) && (point < (l + (h - l + 1) * get_border (t, DEPTH, key, 0) / get_total_weight (t, DEPTH, key)))){
        //printf ("EOF %ld <= %ld < %ld\n", l, point, (l + (h - l + 1) * table[0] / table[256]));
        return EOF;
    }

    for (int i = 0; i <= 255; i++) {
        if (point < (l + (h - l + 1) * get_border (t, DEPTH, key, i+1) / get_total_weight (t, DEPTH, key))) { //if (pointl >= (l + (h - l + 1) * table[i] / table[256]))
            if (point >= (l + (h - l + 1) * get_border (t, DEPTH, key, i) / get_total_weight (t, DEPTH, key))) {
                //printf ("YES %ld %ld %ld %ld, %ld %ld %ld\n", pointl, pointh, (l + (h - l + 1) * table[i] / table[256]), (l + (h - l + 1) * table[i+1] / table[256]), table[i], table[i+1], table[256]);
                //printf ("YES %ld <= %ld < %ld          %c (=%d)\n", (l + (h - l + 1) * table[i] / table[256]), point, (l + (h - l + 1) * table[i+1] / table[256]) - 1, i, i);
                //printf ("YES %ld <= %ld <= %ld          %c (=%d)\n", (l + (h - l + 1) * get_border (t, key, i) / get_total_weight (t, key)), point, (l + (h - l + 1) * get_border (t, key, i+1) / get_total_weight (t, key)) - 1, i, i);
                return i;
            }
            else
                return NOT_EOF_PPM;
        }
    }
    return NOT_EOF_PPM;
}

void decompress_ppm(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    int64_t li = 0, hi = 4294967295/*4611686018427387903*/, l = li, h = hi; /** hi = 2^32 - 1 */
    int64_t point = 0;
    int64_t First_qtr = (hi + 1) / 4, Second_qtr = First_qtr * 2, Third_qtr = First_qtr * 3;
    int64_t table[257]; /** (my eof + 256 symbols) probability gapes = 257 variables per table*/
    int64_t max_weight = (hi + 1) / 4;
    initialize_table_ppm (table);
    tree model = initialize_tree ();
    int bits_to_follow = 0;

    int byte = fgetc (ifp), c;
    for (int i = 0; i < 32; i++) {
        if (byte != EOF) {
            point += point + ((byte >> (7 - i % 8)) & 1);
        }
        else {
            point += point;
        }
        //printf ("i=%d: %d - %ld\n",i, ((byte >> (7 - i % 8)) & 1),point);
        if ((i % 8) == 7)
            byte = fgetc (ifp);
    }

    int last_byte = byte;
    int i = 0;
    int c_num = 0;
    int c_buf[4] = {-1, -1, -1, -1};

    while ((last_byte != EOF) || (c != EOF)) {
        if (c_num < DEPTH - 1) {
            c = get_symbol_ari (point, li, hi, table);
            if (c == EOF)
                    break;

            l = li + (((hi - li + 1) * table[c]) / table[256]);
            h = li + (((hi - li + 1) * table[c+1]) / table[256]) - 1;
            li = l;
            hi = h;
            c_buf[c_num] = c;
            c_num++;
        }
        else {
            c = get_symbol_ppm (point, li, hi, model, c_buf);
            if (c == EOF)
                    break;

            c_buf[3] = c;
            l = li + (((hi - li + 1) * get_border (model, DEPTH, c_buf, c)) / get_total_weight (model, DEPTH, c_buf));
            h = li + (((hi - li + 1) * get_border (model, DEPTH, c_buf, c+1)) / get_total_weight (model, DEPTH, c_buf)) - 1;
            li = l;
            hi = h;

            update_tree (&model, DEPTH, c_buf, 5);

            for (int i = 0; i < DEPTH - 1; i++)
                c_buf[i] = c_buf[i+1];
        }

        while (1) {
            if (hi < Second_qtr) {
                ;
            }
            else if (li >= Second_qtr) {
                li -= Second_qtr;
                hi -= Second_qtr;
                point -= Second_qtr;
            }
            else if ((li >= First_qtr) && (hi < Third_qtr)) {
                li -= First_qtr;
                hi -= First_qtr;
                point -= First_qtr;
            }
            else {
                break;
            }
            li += li;
            hi += hi + 1;
            point += point + ((byte >> (7 - i)) & 1);
            //printf ("bit %d (i=%d), point %ld\n", ((byte >> (7 - i)) & 1), i+1, point);
            i++;
            if ((i % 8) == 0) {
                byte = fgetc(ifp);
                if (byte == EOF) {
                    last_byte = byte;
                    byte = 0;
                }
                i = 0;
                //printf("reading byte %d\n", byte);
            }
        }

        fputc (c, ofp);
    }

    fclose(ifp);
    fclose(ofp);
}
